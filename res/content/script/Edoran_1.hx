globals = {
	abbotStart : false,
	hamletDone : false,
	breakableBridgeDone : false,
	laheartCaravanDefeated : false,
	abbeyAccompanyEnd : false,
	abbeyDead : false,
	abbeyFinished : false,
	hostesGoldStolen : 0,
	hostesDead : false,
	deserterLegion : 0,
	deserterLaheart: 0,
	deserterVernalis : 0,
	desertertrigger : false,
	deserterLegionSeen : false,
	deserterVernalisSeen : false,
	deserterLaheartSeen : false,
	deserterDone : false,
	sabotageAccepted : false,
	sabotageSucceeded : false,
	sabotageAcheived : false,
	sabotageStopped : false,
	sabotageAsercia : false,
	e1LaheartPoints : 0,
	e1VernalisPoints : 0,
	e1ChaosPoints : 0,
	e1LaheartEnd : false,
	e1LaheartAccept : false,
	e1LaheartConclusion : false,
	e1VernalisEnd : false,
	e1VernalisAccept : false,
	e1ChaosEnd : false,
	e1ChaosAccept : false,
	witchFate : false,
	witchSaved : false,
	plagueDead : false,
	plagueAlive : false,
	plagueManMeet : false,
	plagueVillagersMeet : false,
	plagueBarnGive : false,
	plagueBarnConsequence : false,
	plagueBarnKill : false,
	pillageFarmGone : false,
	boughtHorses : 0,
	e1TrackKilled : false,
	ruin1Recruited : false,
	hostesRecruited : false,
	hostesInquisitorDead : false,
};